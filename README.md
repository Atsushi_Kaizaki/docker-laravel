# docker-laravel

## 概要

Laravelプロジェクトを簡易的に始めるための基本環境

## 使い方
### Mac版  
clone後docker-laravelのプロジェクト直下に移動する。
```
cd infrastructure
make create-project
```
これだけでも動作はするが、よく使うパッケージを同時に導入する場合は以下のコマンドを実行する。
```
make install-recommend-packages
```
infrastructureと同じ階層にすでにbackend(Laravelリソース)が存在する場合は以下
```
make init
```
make create-project
```
これだけでも動作はするが、よく使うパッケージを同時に導入する場合は以下のコマンドを実行する。
```
make install-recommend-packages
```
infrastructureと同じ階層にすでにbackend(Laravelリソース)が存在する場合は以下
```

### Windows版
動作前提： Windows10  
1. windowsボタン（スタート）を右クリックしてシステムを選択し、Windowsの仕様欄でバージョンを確認してください。  
  
#### バージョンが19xxの場合、かつWindows10 Homeの場合
1. Vagrantの最新版を[こちら](https://www.vagrantup.com/downloads)からダウンロードしインストールする。(ver2.2.10 2020/10/17現在)
2. VirtualBoxの6.0系最新版を[こちら](https://www.virtualbox.org/wiki/Download_Old_Builds_6_0)からインストールする  
※ VirtualBoxの最新版は6.1.4(2020/10/17現在)ですが、vagrantとの相性が悪かったので6.0.x系の最新版をインストールしています。
3. phpStormやVSCODEのターミナルでcdコマンドを使いvagrantフォルダへ移動します。
4. `vagrant up`コマンドを実行します。
5. `vagrant ssh`でVMにログインし、`cd globalwifi-lfs`で移動します。
6. `docker-compose up -d`を実行します。  
  
次回以降の操作は4.5.6.を実行します。  
終了時は`vagrant halt`を実行します。  
複数のVMがある場合は`vagrant halt globalwifi`を実行します。  

#### バージョンが20xx以降の場合、もしくはWindows10 Proの場合
Mac版のDockerと同じです。

※ Windows版のWSL2（Hyper-Vを使わない）でのDocker環境はまだまだ発展途上で動作不安定な場合があります。うまくいかない場合は、無理せずVagrant＋VirtualBox環境を利用して構築してください。  

#### Windows版のDocker DesktopとVagrant＋VirtualBox環境の共存についての注意事項
現時点(2020/10/17現在)でWindows10 Pro、もしくはWindows 10 home バージョン2004の場合、Hyper-Vが有効になっている。  
OracleのVirtualBoxは基本的にVT-Xで動作するので、基本的にVB6.0系の場合共存はできない。  
管理者モードのPowershellなどで
```
cd "C:\Program Files\Oracle\VirtualBox"
.\VBoxManage.exe setextradata global "VBoxInternal/NEM/UseRing0Runloop" 0
```
とすることで、一応Hyper-VでVMを起動できるオプションが6.0で実装されているが、

- 結構な頻度でカーネルパニックを起こす。
- うまく起動できてもHyper-Vの数倍（体感値だと数十倍）程起動も動作も遅い。
- それでも頑張って使ってもたまにHostとGuestのファイル同期に失敗する。

といったデメリットがあり、やる意味を感じない。  
  
そこで注意としては、次の様になる。

|仮想環境|Hyper-V|VT-X|設定コマンド|
| --- | --- | --- | --- |
|Docker Desktop|On|Off|`bcdedit /set hypervisorlaunchtype on`|
|Vagrant + VirtualBox|Off|On|`bcdedit /set hypervisorlaunchtype off`|

コマンドを管理者権限でPowershellなどで実行した後、端末の再起動で反映される。

clone後プロジェクト直下のvagrantフォルダに移動する。
```
vagrant up
vagrant ssh
```
vagrantに移動後、
```
ll docker-laravel
```
などで中身がマウントされていることを確認
```
cd docker-laravel/infrastructure
make create-project
```


## コンテナ構成

```bash
├── backend
  ├── app
  ├── web
  └── db
```

### app container

- Base image
  - [php](https://hub.docker.com/_/php):7.4-fpm-buster
  - [composer](https://hub.docker.com/_/composer):latest

### web container

- Base image
  - [nginx](https://hub.docker.com/_/nginx):1.18-alpine
  - [node](https://hub.docker.com/_/node):14.2-alpine

### db container

- Base image
  - [mysql](https://hub.docker.com/_/mysql):8.0

(プロジェクトによってミドルウェアの構成は適宜変更してください。)

## TODO
* ローカルでのメールアプリケーション検証のために`mailhog`を導入する。
* API検証のために`postman`のREADMEを作成する。
* Swaggerの導入でSPAでバックエンド対応なしの場合（frontend）のみの場合を考える。
* ミドルウェアの切り替えなど対話式コンソールを実装して楽にする。
* DBインスタンスの初期データ投入など実装する。