# Install Docker & docker-compose
sudo /etc/init.d/docker restart latest
wget -L https://github.com/docker/compose/releases/download/1.25.0-rc2/docker-compose-`uname -s`-`uname -m`
chmod +x docker-compose-`uname -s`-`uname -m`
mv docker-compose-`uname -s`-`uname -m` /opt/bin/docker-compose
chown root:root /opt/bin/docker-compose
echo "alias dc='docker-compose'" > /etc/profile.d/docker-compose.sh
chown root:root /etc/profile.d/docker-compose.sh
chmod 644 /etc/profile.d/docker-compose.sh
# Install curl

